import 'package:flutter_news/models/topic.dart';

List<Topic> topics = [
  Topic('apple', 'https://www.investo.vn/wp-content/uploads/2021/11/overview__bcphzsdb4fpu_og.jpg'),
  Topic('tesla',
      'https://m.media-amazon.com/images/I/41Jr2ga-0xL._AC_SL1125_.jpg'),
  Topic('country',
      'https://elinkgolf.vn/wp-content/uploads/2020/11/Chi-Linh-Star-Golf-Country-Club.jpg'),
  Topic('business',
      'https://nv.edu.vn/wp-content/uploads/2021/06/Business-la-gi-e1623418238104.jpg'),
  Topic('general',
  'https://indianmarketview.com/wp-content/uploads/News-Papers-300x200.jpeg'),
  Topic('sport',
  'https://langgo.edu.vn/public/files/upload/default/medium/images/sport-equipment-concept-1284-13034.jpg'),
  Topic('music',
  'https://ctmobile.vn/upload/dims.jpg'),
];
